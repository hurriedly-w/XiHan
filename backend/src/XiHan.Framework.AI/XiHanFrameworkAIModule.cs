﻿#region <<版权版本注释>>

// ----------------------------------------------------------------
// Copyright ©2024 ZhaiFanhua All Rights Reserved.
// Licensed under the MulanPSL2 License. See LICENSE in the project root for license information.
// FileName:XiHanFrameworkAIModule
// Guid:b199f478-3f76-4fa4-854e-11e6682c62da
// Author:Administrator
// Email:me@zhaifanhua.com
// CreateTime:2024-04-28 下午 01:25:54
// ----------------------------------------------------------------

#endregion <<版权版本注释>>

using XiHan.Framework.Core.Modularity;

namespace XiHan.Framework.AI;

/// <summary>
/// 曦寒框架人工智能组件库
/// </summary>
public partial class XiHanFrameworkAIModule : XiHanModule
{
    /// <summary>
    /// 配置服务
    /// </summary>
    /// <param name="context"></param>
    public override void ConfigureServices(ServiceConfigurationContext context)
    {
    }
}
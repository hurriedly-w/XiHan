﻿#region <<版权版本注释>>

// ----------------------------------------------------------------
// Copyright ©2024 ZhaiFanhua All Rights Reserved.
// Licensed under the MulanPSL2 License. See LICENSE in the project root for license information.
// FileName:XiHanFrameworkPersistenceModule
// Guid:baa046da-681d-478e-bd3e-eead2f3dcbe8
// Author:zhaifanhua
// Email:me@zhaifanhua.com
// CreateTime:2024/3/28 9:36:32
// ----------------------------------------------------------------

#endregion <<版权版本注释>>

using XiHan.Framework.Core.Modularity;

namespace XiHan.Infrastructure.Persistence;

/// <summary>
/// 基础设施数据持久化模块初始化
/// </summary>
public class XiHanFrameworkPersistenceModule : XiHanModule
{
}
﻿#region <<版权版本注释>>

// ----------------------------------------------------------------
// Copyright ©2024 ZhaiFanhua All Rights Reserved.
// Licensed under the MulanPSL2 License. See LICENSE in the project root for license information.
// FileName:XiHanFrameworkEventBusModule
// Guid:0bc89149-d0df-467f-a3be-c571b00af610
// Author:zhaifanhua
// Email:me@zhaifanhua.com
// CreateTime:2024/3/28 9:25:06
// ----------------------------------------------------------------

#endregion <<版权版本注释>>

using XiHan.Framework.Core.Modularity;

namespace XiHan.Framework.EventBus;

/// <summary>
/// 曦寒框架集成事件模块
/// </summary>
public class XiHanFrameworkEventBusModule : XiHanModule
{
}